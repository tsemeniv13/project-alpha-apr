from django.urls import path
from .views import lst_projects, project_details, create_project

urlpatterns = [
    path("", lst_projects, name="list_projects"),
    path("<int:id>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
