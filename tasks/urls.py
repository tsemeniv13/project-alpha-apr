from django.urls import path
from .views import create_task, lst_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", lst_tasks, name="show_my_tasks"),
]
